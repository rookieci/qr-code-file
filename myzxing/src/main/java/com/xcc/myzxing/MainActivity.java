package com.xcc.myzxing;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.Result;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;

import java.util.Hashtable;

public class MainActivity extends Activity {
    private final static int SCANNIN_GREQUEST_CODE = 1;
    /**
     * 显示扫描结果
     */
    private TextView mTextView;
    /**
     * 显示扫描拍的图片
     */
    private ImageView mImageView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zx_activity_main_zxing);
        mTextView = (TextView) findViewById(R.id.result);
        mImageView = (ImageView) findViewById(R.id.qrcode_bitmap);

        Button mButton = (Button) findViewById(R.id.button1);
        mButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, MipcaActivityCapture.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivityForResult(intent, SCANNIN_GREQUEST_CODE);
            }
        });
        mButton.setOnLongClickListener(new View.OnLongClickListener() {
            @SuppressLint("NewApi")
            public boolean onLongClick(View v) {
                Bitmap scanBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.qq27);
                Log.v("scanBitmap-Width-Height", scanBitmap.getWidth() + "-"
                        + scanBitmap.getHeight());
                try {
                    mImageView.setImageBitmap(scanBitmap);

                    Hashtable<DecodeHintType, String> hints = new Hashtable<DecodeHintType, String>();
                    hints.put(DecodeHintType.CHARACTER_SET, "utf-8"); // 设置二维码内容的编码
                    BitmapLuminanceSource source = new BitmapLuminanceSource(scanBitmap);
                    BinaryBitmap bitmap1 = new BinaryBitmap(new HybridBinarizer(source));
                    QRCodeReader reader = new QRCodeReader();
                    Result decode = reader.decode(bitmap1, hints);

                    // 显示扫描到的内容
                    mTextView.setText(decode.getText());
                    // 显示
                    mImageView.setImageBitmap(scanBitmap);
                } catch (Exception e) {
                    e.printStackTrace();
                    mTextView.setText("出错");
                }
                return false;
            }
        });
        mImageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Bitmap create2dCode = Create2DCode("磁磁帅sdhfiuasdgoigjkfb8oiu");
                    mImageView.setImageBitmap(create2dCode);
                    mTextView.setText("");
                } catch (Exception e) {
                    mTextView.setText("出错");
                }
            }
        });
    }

    public Bitmap Create2DCode(String str) throws Exception {
        Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8"); // 设置二维码内容的编码
        // 生成二维矩阵,编码时指定大小,不要生成了图片以后再进行缩放,这样会模糊导致识别失败
        BitMatrix matrix = new MultiFormatWriter().encode(str,
                BarcodeFormat.QR_CODE, 600, 600, hints);
        int width = matrix.getWidth();
        int height = matrix.getHeight();
        // 二维矩阵转为一维像素数组,也就是一直横着排了
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (matrix.get(x, y)) {
                    pixels[y * width + x] = 0xff000000;
                }
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        // 通过像素数组生成bitmap,具体参考api
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case SCANNIN_GREQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    // 显示扫描到的内容
                    mTextView.setText(bundle.getString("result"));
                    // 显示
                    //mImageView.setImageBitmap((Bitmap) data.getParcelableExtra("bitmap"));
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }
}