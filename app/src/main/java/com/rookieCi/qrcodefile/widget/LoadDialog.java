package com.rookieCi.qrcodefile.widget;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.widget.TextView;

import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar;
import com.rookieCi.qrcodefile.R;

public class LoadDialog extends Dialog {
    private TextView txtLoading;
    private CircleProgressBar progressBar;

    public LoadDialog(Context context) {
        super(context, R.style.AlertDialogStyle);
        //this.context = context;
        setCancelable(true);
        setCanceledOnTouchOutside(false);

        setContentView(R.layout.dialog_load);

        txtLoading = (TextView) findViewById(R.id.txt_loading);
        progressBar = (CircleProgressBar) findViewById(R.id.progressBar);
        //progressBar.setShowArrow(true);
        progressBar.setCircleBackgroundEnabled(false);
        progressBar.setColorSchemeResources(R.color.purple_200, R.color.purple_500, R.color.purple_700, R.color.teal_200, R.color.teal_700);
    }

    public LoadDialog setColorSchemeResources(int... colorRes) {
        progressBar.setColorSchemeResources(colorRes);
        return this;
    }

    public LoadDialog setText(String text) {
        if (!TextUtils.isEmpty(text)) txtLoading.setText(text);
        return this;
    }

    public LoadDialog setText(int textId) {
        txtLoading.setText(textId);
        return this;
    }
}
