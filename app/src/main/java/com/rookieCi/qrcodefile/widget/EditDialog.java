package com.rookieCi.qrcodefile.widget;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.material.textfield.TextInputEditText;
import com.rookieCi.qrcodefile.R;
import com.rookieCi.qrcodefile.sync.SendFileContact;
import com.rookieCi.qrcodefile.utils.UserConfig;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditDialog extends Dialog {
    @BindView(R.id.edit)
    public TextInputEditText edit;
    private Context context;

    public EditDialog(@NonNull Context context) {
        super(context);
        //super(context, R.style.AlertDialogStyle_);
        this.context = context;
        //setCancelable(true);
        //setCanceledOnTouchOutside(false);

        setContentView(R.layout.dialog_edit);
        //setOnKeyListener(QrApp.onKeyListener);

        ButterKnife.bind(this);

        int codeLen = SendFileContact.codeLen;
        edit.setText(String.valueOf(codeLen));
    }

    @OnClick(R.id.button)
    public void onButtonClick() {
        String s = edit.getText().toString();
        if (TextUtils.isEmpty(s)) return;
        int anInt;
        try {
            anInt = Integer.parseInt(s);
        } catch (Exception e) {
            Toast.makeText(context, "不要瞎填", Toast.LENGTH_SHORT).show();
            return;
        }
        if (anInt < 50) {
            Toast.makeText(context, "最小值50", Toast.LENGTH_SHORT).show();
        } else if (anInt > 500) {
            Toast.makeText(context, "太大识别不了", Toast.LENGTH_SHORT).show();
        } else {
            SendFileContact.codeLen = anInt;
            UserConfig.setSendNumb(anInt);
            dismiss();
        }
    }
}
