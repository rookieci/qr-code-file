package com.rookieCi.qrcodefile.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.rookieCi.qrcodefile.R;

/**
 * Created by 磁磁帅 on 2020/2/6.
 * 功能：
 */
public class RatioFrameLayout extends FrameLayout {
    /**
     * 以X为基准进行比例缩放
     */
    public static final int ASPECT_RATION_BASE_X = 0;
    /**
     * 以Y为基准进行比例缩放
     */
    public static final int ASPECT_RATION_BASE_Y = 1;

    private float aspectRatio = 0;
    private int aspectRatioBase = 0;

    public RatioFrameLayout(@NonNull Context context) {
        super(context);
    }

    public RatioFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        readAttrs(context, attrs);
    }

    public RatioFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        readAttrs(context, attrs);
    }

    private void readAttrs(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.RatioFrameLayout);
        aspectRatio = a.getFloat(R.styleable.RatioFrameLayout_aspectRatio, 0.0f);
        aspectRatioBase = a.getInteger(R.styleable.RatioFrameLayout_aspectRatioBase, ASPECT_RATION_BASE_X);
        a.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (aspectRatio <= 0) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            return;
        }
        setMeasuredDimension(getDefaultSize(0, widthMeasureSpec), getDefaultSize(0, heightMeasureSpec));
        if (aspectRatioBase == ASPECT_RATION_BASE_Y) {
            int childHeightSize = getMeasuredHeight();
            widthMeasureSpec = MeasureSpec.makeMeasureSpec((int) (childHeightSize * aspectRatio), MeasureSpec.EXACTLY);
            heightMeasureSpec = MeasureSpec.makeMeasureSpec((childHeightSize), MeasureSpec.EXACTLY);
        } else {
            int childWidthSize = getMeasuredWidth();
            widthMeasureSpec = MeasureSpec.makeMeasureSpec(childWidthSize, MeasureSpec.EXACTLY);
            heightMeasureSpec = MeasureSpec.makeMeasureSpec((int) (childWidthSize / aspectRatio), MeasureSpec.EXACTLY);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

    }

    /**
     * 得到长宽比例默认为1：1，当设置比例小于0的时候，即不使用比例
     *
     * @return the aspectRatio
     */
    public float getAspectRatio() {
        return aspectRatio;
    }

    /**
     * 设置ImageView的长宽只比
     *
     * @param aspectRatio the aspectRatio to set
     */
    public void setAspectRatio(float aspectRatio) {
        this.aspectRatio = aspectRatio;
        this.postInvalidate();
    }

    /**
     * @return the aspectRatioBase
     */
    public int getAspectRatioBase() {
        return aspectRatioBase;
    }

    /**
     * @param aspectRatioBase the aspectRatioBase to set
     */
    public void setAspectRatioBase(int aspectRatioBase) {
        this.aspectRatioBase = aspectRatioBase;
        this.postInvalidate();
    }
}
