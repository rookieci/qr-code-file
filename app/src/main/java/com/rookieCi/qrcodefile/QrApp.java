package com.rookieCi.qrcodefile;

import android.app.Application;
import android.content.DialogInterface;
import android.os.Handler;
import android.view.KeyEvent;

import java.util.Random;

public class QrApp extends Application {
    public static final DialogInterface.OnKeyListener onKeyListener = (dialog, keyCode, event) -> keyCode == KeyEvent.KEYCODE_BACK;
    private static QrApp qrApp;

    public static QrApp getInstance() {
        return qrApp;
    }

    private Handler handler;

    @Override
    public void onCreate() {
        super.onCreate();
        qrApp = this;

        handler = new Handler();
    }

    public void runOnUiThread(Runnable r) {
        handler.post(r);
    }

    public void runOnUiThread(Runnable r, long delayMillis) {
        handler.postDelayed(r, delayMillis);
    }
}
