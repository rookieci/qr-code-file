package com.rookieCi.qrcodefile;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.rookieCi.qrcodefile.quick.SendFile2Activity;
import com.rookieCi.qrcodefile.sync.AcceptFileActivity;
import com.rookieCi.qrcodefile.sync.SendFileActivity;
import com.rookieCi.qrcodefile.utils.ZipUtils;
import com.rookieCi.qrcodefile.widget.EditDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.edit)
    public TextInputEditText edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        /*edit.postDelayed(() -> edit.setText("明月几时有，把酒问青天。不知天上宫阙，今夕是何年。我欲乘风归去，又恐琼楼玉宇，高处不胜寒。起舞弄清影，何似在人间。" +
                "转朱阁，低绮户，照无眠。不应有恨，何事长向别时圆？人有悲欢离合，月有阴晴圆缺，此事古难全。但愿人长久，千里共婵娟。"), 100);*/
    }

    @OnClick(R.id.button)
    public void onButtonClick() {//发送
        String s = edit.getText().toString();
        if (TextUtils.isEmpty(s)) return;
        String zip = ZipUtils.zip(s);
        int length = zip == null || zip.length() == 0 ? Integer.MAX_VALUE : zip.length();
        Intent intent = new Intent(this, SendFileActivity.class);
        if (length < s.length()) {
            intent.putExtra("sendType", 3);
            intent.putExtra("text", zip);
        } else {
            intent.putExtra("sendType", 1);
            intent.putExtra("text", s);
        }
        startActivity(intent);
    }

    @OnClick(R.id.button2)
    public void onButton2Click() {//扫码接收
        Intent intent = new Intent(this, AcceptFileActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.button3)
    public void onButton3Click() {//快速发送
        String s = edit.getText().toString();
        if (TextUtils.isEmpty(s)) return;
        String zip = ZipUtils.zip(s);
        int length = zip == null || zip.length() == 0 ? Integer.MAX_VALUE : zip.length();
        Intent intent = new Intent(this, SendFile2Activity.class);
        if (length < s.length()) {
            intent.putExtra("sendType", 3);
            intent.putExtra("text", zip);
        } else {
            intent.putExtra("sendType", 1);
            intent.putExtra("text", s);
        }
        startActivity(intent);
    }

    @OnClick(R.id.button4)
    public void onButton4Click() {//快速接收
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.menuBtn) {
            EditDialog dialog = new EditDialog(this);
            dialog.show();
        }
        return super.onOptionsItemSelected(item);
    }
}