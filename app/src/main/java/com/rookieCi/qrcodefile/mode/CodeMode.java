package com.rookieCi.qrcodefile.mode;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.rookieCi.qrcodefile.sync.SendFileContact;

import java.util.Random;

public class CodeMode {
    public final static String[] CodeEndStr = {"但愿人长久，千里共婵娟", "仰天大笑出门去，我辈岂是蓬蒿人"
            , "大鹏一日同风起，扶摇直上九万里", "达则兼济天下，穷则独善其身", "今朝有酒今朝醉，明日愁来明日愁"
            , " 精诚所至，金石为开", "天若有情天亦老，人间正道是沧桑", "一身转战三千里，一剑曾挡百万师"
            , "海内存知己，天涯若比邻", "工欲善其事，必先利其器", "犬之颠，傲世间，看我黑皇咬死仙", "我儿王腾有大帝之姿"
            , "仙路尽头谁为峰，一见无始道成空", "仙之巅，傲世间，由我安澜便有天", "哪怕背负天渊，需一手托原始帝城，我安澜一样无敌世间"
            , "是人是鬼都在秀,只有骨哥在挨揍", "我一介凡人！ 我，要不朽", "不要回答，不要回答，不要回答", "不要返航，这里不是家"
            , "毁灭你，与你何干"};
    public int idx;//-1表示结束
    public int t;//0-文件，1文本，2-压缩文件，3-压缩文本
    public int no;
    public String str;

    public CodeMode(SendFileContact fileContact) {
        if (fileContact == null) {
            idx = -1;
            int i = new Random().nextInt(CodeEndStr.length);
            str = CodeEndStr[i];
        } else {
            t = fileContact.getSendType();
            no = fileContact.getNo();
            str = fileContact.getCodeString();

            if (TextUtils.isEmpty(str)) {
                idx = -1;
                int i = new Random().nextInt(CodeEndStr.length);
                str = CodeEndStr[i];
            } else {
                idx = fileContact.getCodeInt();
            }
        }
    }

    public CodeMode() {
    }

    public String getJsonString() {
        return new Gson().toJson(this);
    }
}
