package com.rookieCi.qrcodefile.sync;

public interface OnFileContactInterface {
    /**
     * 请求文件权限
     */
    boolean checkFilePermissions();
}
