package com.rookieCi.qrcodefile.utils;

import android.content.SharedPreferences;

import com.rookieCi.qrcodefile.QrApp;

/**
 * Created by 52463 on 2018/3/13.
 */

public class UserConfig {
    private static final String UserMsg = "qrcodefile_d";

    public static void setSendNumb(int sendNumb) {
        SharedPreferences sp = QrApp.getInstance().getSharedPreferences(UserMsg, 0);
        sp.edit().putInt("sendNumb", sendNumb).apply();
    }

    public static int getSendNumb() {
        SharedPreferences sp = QrApp.getInstance().getSharedPreferences(UserMsg, 0);
        return sp.getInt("sendNumb", 80);
    }
}
