package com.rookieCi.qrcodefile.utils;

import android.app.Activity;

import com.rookieCi.qrcodefile.widget.LoadDialog;

public class LoadDialogUtils {
    private final Activity activity;
    private LoadDialog loadDialog;

    public LoadDialogUtils(Activity activity) {
        this.activity = activity;
    }

    public void openLoadDialog() {
        openLoadDialog(null, false);
    }

    public void openLoadDialog(String text) {
        openLoadDialog(text, false);
    }

    public void openLoadDialog(String text, boolean notClose) {
        if (activity.isDestroyed() || activity.isFinishing()) return;
        if (loadDialog != null) {
            if (loadDialog.isShowing()) return;
        }
        if (loadDialog == null) {
            loadDialog = new LoadDialog(activity)
                    .setText(text);
            if (notClose) {
                loadDialog.setCanceledOnTouchOutside(false);
                loadDialog.setCancelable(false);
            }
            loadDialog.setOnDismissListener(dialog -> {
                if (loadDialog == dialog) loadDialog = null;
            });
        }
        loadDialog.show();
    }

    public void dismissDialog() {
        if (activity.isDestroyed() || activity.isFinishing()) return;
        if (loadDialog == null) return;
        loadDialog.dismiss();
    }
}
