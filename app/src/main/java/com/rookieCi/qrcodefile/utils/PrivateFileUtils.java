package com.rookieCi.qrcodefile.utils;

import android.content.Context;
import android.text.TextUtils;

import com.rookieCi.qrcodefile.BuildConfig;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Created by 52463 on 2018/3/13.
 */
public class PrivateFileUtils {
    public PrivateFileUtils(Context context, String jsonName) {
        this.context = context;
        this.jsonName = jsonName;
    }

    private final String jsonName;
    private final Context context;

    public String getFilePath() {
        return FileOperateUtil.getPrivatePath(context) + File.separator + jsonName;
    }

    public String getString() {
        try {
            FileInputStream inputStream = context.openFileInput(jsonName);
            byte[] bs = new byte[1024];
            int idx;
            StringBuilder txt = new StringBuilder();
            while ((idx = inputStream.read(bs)) > 0) {
                txt.append(new String(bs, 0, idx));
            }
            inputStream.close();
            return txt.toString();
        } catch (Exception e) {
        }
        return null;
    }

    public boolean setString(String string) {
        byte[] bytes = string.getBytes();
        return setByte(bytes);
    }

    public byte[] getByte() {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            FileInputStream inputStream = context.openFileInput(jsonName);
            byte[] bs = new byte[1024];
            int idx;
            while ((idx = inputStream.read(bs)) > 0) {
                outputStream.write(bs, 0, idx);
            }
            inputStream.close();
            byte[] bytes = outputStream.toByteArray();
            outputStream.close();
            return bytes;
        } catch (Exception e) {
            if (BuildConfig.DEBUG) e.printStackTrace();
        }
        return null;
    }

    /**
     * @return 返回写入结果
     */
    public boolean setByte(byte[] bytes) {
        try {
            FileOutputStream fileOutputStream = context.openFileOutput(jsonName, 0);
            fileOutputStream.write(bytes);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (Exception e) {
            if (BuildConfig.DEBUG) e.printStackTrace();
            return false;
        }
        return true;
    }

    public void delete() {
        if (TextUtils.isEmpty(jsonName)) return;
        File f = new File(context.getFilesDir(), jsonName);
        if (f.exists()) {
            f.delete();
        }
    }

    /**
     * 删除以jsonName开头的文件
     */
    public void deleteAllStart() {
        if (TextUtils.isEmpty(jsonName)) return;
        File filesDir = context.getFilesDir();
        File[] files = filesDir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) continue;
            String name = file.getName();
            if (TextUtils.isEmpty(name)) continue;
            if (name.startsWith(jsonName)) {
                file.delete();
            }
        }
    }

    public static void deleteAllEnd(Context context, String end) {
        if (TextUtils.isEmpty(end)) return;
        File filesDir = context.getFilesDir();
        File[] files = filesDir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) continue;
            String name = file.getName();
            if (TextUtils.isEmpty(name)) continue;
            if (name.endsWith(end)) {
                file.delete();
            }
        }
    }
}