package com.rookieCi.qrcodefile.utils;

import android.text.TextUtils;
import android.util.Log;

import com.rookieCi.qrcodefile.BuildConfig;

/**
 * Created by xcc on 2016/11/21.
 */
public class Logger {
    public static void i(Object object, String msg) {
        if (BuildConfig.DEBUG) {
            String s = object.toString();
            if (TextUtils.isEmpty(s) || TextUtils.isEmpty(msg))
                return;
            Log.i(s, msg);
        }
    }

    public static void e(Object object, String msg) {
        if (BuildConfig.DEBUG) {
            String s = object.toString();
            if (TextUtils.isEmpty(s) || TextUtils.isEmpty(msg))
                return;
            Log.e(object.toString(), msg);
        }
    }

    public static void d(Object object, String msg) {
        if (BuildConfig.DEBUG) {
            String s = object.toString();
            if (TextUtils.isEmpty(s) || TextUtils.isEmpty(msg))
                return;
            Log.d(object.toString(), msg);
        }
    }

    public static void log(String tag, String msg) {
        v(tag, msg);
    }

    public static void v(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            if (TextUtils.isEmpty(tag) || TextUtils.isEmpty(msg))
                return;
            Log.v(tag, msg);
        }
    }

    public static void out(String msg) {
        if (BuildConfig.DEBUG && !TextUtils.isEmpty(msg)) {
            //长度大于3000，分割
            final int len = 3000;
            if (msg.length() > len) {
                for (int i = 1; ; i++) {
                    int i1 = (i - 1) * len;
                    int i2 = i1 + len;
                    if (i2 > msg.length()) {
                        i("-xcc-System.out", msg.substring(i1, msg.length()));
                        break;
                    } else i("-xcc-System.out", msg.substring(i1, i2));
                }
            } else i("-xcc-System.out", msg);
        }
    }

    public static void println(String msg) {
        out(msg);
    }

    public static void w(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            if (TextUtils.isEmpty(tag) || TextUtils.isEmpty(msg))
                return;
            Log.w(tag, msg);
        }
    }

    public static void w(String logTag, InterruptedException e) {
        if (BuildConfig.DEBUG) {
            if (TextUtils.isEmpty(logTag) || e == null)
                return;
            Log.w(logTag, e);
        }
    }
}
