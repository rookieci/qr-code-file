package com.rookieCi.qrcodefile.quick;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;

import com.rookieCi.qrcodefile.BuildConfig;
import com.rookieCi.qrcodefile.QrApp;
import com.rookieCi.qrcodefile.mode.CodeMode;
import com.rookieCi.qrcodefile.sync.OnFileContactInterface;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class SendFileContact {
    /**
     * 0-文件，1文本，2-压缩文件，3-压缩文本
     */
    private int sendType;
    private String text;//sendType=1时使用，最大长度100个字
    private int no;
    private int index;//光标位置
    private String filePath;//文件路径
    private FileInputStream inputStream;//sendType=2时使用

    public SendFileContact(Intent intent, OnFileContactInterface contactInterface) {
        //this.contactInterface = contactInterface;
        long l = System.currentTimeMillis();
        no = (int) (l % 10000);
        index = -1;

        sendType = intent.getIntExtra("sendType", 0);
        if (sendType == 1) {
            text = intent.getStringExtra("text");
        } else if (sendType == 3) {
            text = intent.getStringExtra("text");
        } else if (sendType == 2) {
            /*filePath = intent.getStringExtra("text");
            if (contactInterface != null) {
                boolean b = contactInterface.checkFilePermissions();
                if (b) openFile();
            }*/
        } else if (intent.getAction().equals(Intent.ACTION_SEND)) {//文件
            /*Bundle bundle = intent.getExtras();
            if (bundle == null) return;
            Uri uri = (Uri) bundle.get(Intent.EXTRA_STREAM);
            if (uri == null) return;//content://media/external/file/50489
            filePath = getRealPathFromUri(QrApp.getInstance(), uri);
            if (contactInterface != null) {
                boolean b = contactInterface.checkFilePermissions();
                if (b) openFile();
            }*/
        }
    }

    public int getSendType() {
        return sendType;
    }

    private List<String> textList;//仅文本时使用

    private void initTextList() {
        if (textList == null) {
            textList = new ArrayList<>();
            int codeLen = com.rookieCi.qrcodefile.sync.SendFileContact.codeLen;
            if (text != null && text.length() > codeLen) {
                for (int i = 1; ; i++) {
                    int i1 = (i - 1) * codeLen;
                    int i2 = i1 + codeLen;
                    if (i2 > text.length()) {
                        String substring = text.substring(i1);
                        textList.add(substring);
                        break;
                    } else {
                        String substring = text.substring(i1, i2);
                        textList.add(substring);
                    }
                }
            } else textList.add(text);
        }
    }

    public int getLen() {
        if (sendType == 1 || sendType == 3) {
            initTextList();
            return textList == null ? 0 : textList.size();
        } else {//2、0

        }
        return 0;
    }

    /**
     * @return 光标位置 -1表示开始位置
     */
    public int getIndex() {
        return index;
    }

    public void moveNext() {
        index++;
        if (sendType == 1 || sendType == 3) {
            if (textList == null || textList.size() == 0 || index >= textList.size()) {
                index = 0;
            }
        } else {

        }
    }

    public String getCodeString() {
        if (sendType == 1 || sendType == 3) {
            initTextList();
            if (textList == null || textList.size() == 0 || index < 0 || index >= textList.size()) {
                final String[] CodeEndStr = CodeMode.CodeEndStr;
                int i = new Random().nextInt(CodeEndStr.length);
                return CodeEndStr[i];
            } else return textList.get(index);
        } else {//文件
            /*if (inputStream == null) return null;
            if (codeInt == 0) {//最开始将文件名发送过去
                File file = new File(filePath);
                return file.getName();
            }
            try {
                byte[] bytes = new byte[codeLen];
                int read = inputStream.read(bytes);
                if (read < 0) {//文件读取完成
                    inputStream.close();
                    inputStream = null;
                    return null;
                }
                String base64;
                if (read == bytes.length) {
                    base64 = Base64.encodeToString(bytes, Base64.DEFAULT);
                } else {
                    byte[] bs = Arrays.copyOf(bytes, read);
                    base64 = Base64.encodeToString(bs, Base64.DEFAULT);
                }
                return base64;
            } catch (IOException e) {
                if (BuildConfig.DEBUG) e.printStackTrace();
            }*/
            return null;
        }
    }
}
