package com.rookieCi.qrcodefile.quick;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.rookieCi.qrcodefile.BuildConfig;
import com.rookieCi.qrcodefile.QrApp;
import com.rookieCi.qrcodefile.R;
import com.rookieCi.qrcodefile.sync.OnFileContactInterface;
import com.rookieCi.qrcodefile.utils.CodeUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SendFile2Activity extends AppCompatActivity implements OnFileContactInterface {
    private static final int changeTime = 500;
    @BindView(R.id.image)
    public ImageView image;
    @BindView(R.id.button)
    public Button button;
    @BindView(R.id.textContent)
    public TextView textContent;
    private SendFileContact fileContact;
    private boolean isStop;
    private Runnable runnable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_file2);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        fileContact = new SendFileContact(intent, this);

        image.postDelayed(this::initSowCode, 100);
    }

    @Override
    public void onBackPressed() {
        isStop = true;
        super.onBackPressed();
    }

    private void initSowCode() {
        //if (requestFilePermissions) return;
        showCode();
        /*if (fileContact.getSendType() == 0) {
            if (fileContact.isOpenFile()) return;
            new AlertDialog.Builder(SendFileActivity.this)
                    .setTitle("呵呵哒")
                    .setMessage("加载文件失败")
                    .setPositiveButton("知道了", null)
                    .show();
        }*/
    }

    private void showCode() {
        //-1 第一张 长度、类型、文件名
        if (fileContact == null) return;
        SendMode codeMode = new SendMode(fileContact);
        String text = codeMode.getJsonString();
        textContent.setText(codeMode.i == -1 ? "=====准备完成就点击发送吧=====" : text);
        try {
            Bitmap bitmap = new CodeUtils().Create2DCode(text);
            image.setImageBitmap(bitmap);
        } catch (Exception e) {
            if (BuildConfig.DEBUG) e.printStackTrace();
        }
    }

    private void runShowCode() {
        runnable = new Runnable() {
            @Override
            public void run() {
                if (isStop || isDestroyed() || isFinishing()) return;
                if (runnable != this) return;
                if (fileContact != null) fileContact.moveNext();
                showCode();
                if (fileContact != null) {
                    int len = fileContact.getLen();
                    if (len <= 1) return;
                }
                runShowCode();
            }
        };
        QrApp.getInstance().runOnUiThread(runnable, changeTime);
    }

    @OnClick(R.id.button)
    public void onButtonClick() {//发送
        if (runnable == null) runShowCode();
    }

    @Override
    public boolean checkFilePermissions() {
        return false;
    }
}
