package com.rookieCi.qrcodefile.quick;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.rookieCi.qrcodefile.mode.CodeMode;

import java.util.Random;

public class SendMode {
    public int i;//-1表示开始
    public int t;//0-文件，1文本，2-压缩文件，3-压缩文本
    public int l;//长度，指多少帧
    public String s;

    public SendMode(SendFileContact fileContact) {
        final String[] CodeEndStr = CodeMode.CodeEndStr;
        t = fileContact.getSendType();
        l = fileContact.getLen();
        i = fileContact.getIndex();
        s = i == -1 ? null : fileContact.getCodeString();
        if (TextUtils.isEmpty(s)) {
            int i = new Random().nextInt(CodeEndStr.length);
            s = CodeEndStr[i];
        }
    }

    public String getJsonString() {
        return new Gson().toJson(this);
    }
}
